<h4>Изменить статус заказов:<h4>
<select name="status_change" class="cm-submit  cm-skip-validation" data-ca-dispatch="dispatch[my_select.status_change]">
   <option value="P" {if $orders.status_change == "P"}selected="selected"{/if}>{$order_status_descr['P']}</option>
   <option value="C" {if $orders.status_change == "C"}selected="selected"{/if}>{$order_status_descr['C']}</option>
   <option value="O" {if $orders.status_change == "O"}selected="selected"{/if}>{$order_status_descr['O']}</option>
   <option value="F" {if $orders.status_change == "F"}selected="selected"{/if}>{$order_status_descr['F']}</option>
   <option value="D" {if $orders.status_change == "D"}selected="selected"{/if}>{$order_status_descr['D']}</option>
   <option value="B" {if $orders.status_change == "B"}selected="selected"{/if}>{$order_status_descr['B']}</option>
   <option value="I" {if $orders.status_change == "I"}selected="selected"{/if}>{$order_status_descr['I']}</option>
   <option value="Y" {if $orders.status_change == "Y"}selected="selected"{/if}>{$order_status_descr['Y']}</option>
   <option value="X" {if $orders.status_change == "X"}selected="selected"{/if}>{$order_status_descr['X']}</option>
   <option value="W" {if $orders.status_change == "W"}selected="selected"{/if}>{$order_status_descr['W']}</option>
   <option value="A" {if $orders.status_change == "A"}selected="selected"{/if}>{$order_status_descr['A']}</option>
   <option value="E" {if $orders.status_change == "E"}selected="selected"{/if}>{$order_status_descr['E']}</option>
</select>
